package edu.westga.cs1302.animalresort.resources;

/**
 * The Class UIStrings defines output strings that are displayed for the user to
 * see.
 * 
 * @author	CS1302
 * @version Fall 2021
 */
public final class UI {

	public static final String NAME_CANNOT_BE_NULL = "Name cannot be null.";
	public static final String NAME_CANNOT_BE_EMPTY = "Name cannot be empty.";
	public static final String AGE_CANNOT_BE_NEGATIVE = "Age cannot be a negative value.";
	public static final String PET_CANNOT_BE_NULL = "Pet cannot be null.";
	public static final String TYPE_CANNOT_BE_NULL = "Pet type cannot be null.";
	public static final String TYPE_INVALID = "Type must contain a valid pet type.";
	public static final String PETS_CANNOT_BE_NULL = "Pet collection cannot be null.";
	public static final String PETS_CANNOT_CONTAIN_NULL = "Pet collection cannot contain null element.";
}
