package edu.westga.cs1302.animalresort.model;

/**
 * The Class Dog.
 * 
 * @author	CS1302
 * @version Fall 2021
 */
public class Dog extends Pet {

	private boolean dirty;

	/**
	 * Instantiates a new pet.
	 * 
	 * @precondition name != null && name not empty
	 * @postcondition getName() == name && getAge() == age && isHungry() == false &&
	 *                isTired() == false && isBored() == false && isDirty() == false
	 * 
	 * @param name the name
	 * @param age  the age
	 */
	public Dog(String name, int age) {
		super(name, age);
		this.dirty = false;
	}

	/**
	 * Checks if the dog tired.
	 *
	 * @precondition none
	 * @postcondition none
	 * @return true, if is tired
	 */
	public boolean isDirty() {
		return this.dirty;
	}

	/**
	 * Sets whether the dog is dirty.
	 * 
	 * @precondition none
	 * @postcondition isDirty() == dirty
	 * @param dirty - whether pet is dirty
	 */
	public void setDirty(boolean dirty) {
		this.dirty = dirty;
	}

	/**
	 * Returns the sound made by the dog.
	 * 
	 * @precondition none
	 * @postcondition none
	 * 
	 * @return the sound
	 */
	public String getSound() {
		if (this.getAge() < 1) {
			return "yip yip";
		} else {
			return "wuff wuff";
		}
	}

	/**
	 * Spend night.
	 */
	public void spendNight() {
		this.sleep();
	}

	/**
	 * Spend day.
	 */
	public void spendDay() {
		this.eat();
		this.play();
	}

	@Override
	public void play() {
		super.play();
		this.setTired(true);
		this.dirty = true;
	}

	@Override
	public String getReport() {
		String report = super.getReport();
		if (this.isDirty()) {
			report += "* I am dirty." + System.lineSeparator();
		} else {
			report += "* I am clean." + System.lineSeparator();
		}
		return report;
	}

	@Override
	public String toString() {
		return "Dog: " + this.getName() + " (age " + this.getAge() + ")";
	}
}
