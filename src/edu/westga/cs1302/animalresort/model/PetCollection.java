package edu.westga.cs1302.animalresort.model;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

import edu.westga.cs1302.animalresort.resources.UI;

/**
 * The Class PetCollection.
 * 
 * @author	CS1302
 * @version Fall 2021
 */
public class PetCollection implements Collection<Pet> {

	private ArrayList<Pet> pets;

	/**
	 * Instantiates a new pet collection.
	 * 
	 * @precondition none
	 * @postcondition size() == 0
	 */
	public PetCollection() {
		this.pets = new ArrayList<Pet>();
	}
	
	/**
	 * Instantiates a new pet collection.
	 * 
	 * @precondition none
	 * @postcondition size() == 0
	 * @param pets the pets in the collection
	 */
	public PetCollection(Collection<Pet> pets) {
		this.pets = new ArrayList<Pet>();
		for (Pet pet : pets) {
			this.pets.add(pet);
		}
	}

	/**
	 * Gets the size.
	 * 
	 * @precondition none
	 * @postcondition none
	 * @return the size of this pet collection
	 */
	public int size() {
		return this.pets.size();
	}

	/**
	 * Adds the specified pet to this collection.
	 * 
	 * @precondition pet != null
	 * @postcondition size() = size()@prev + 1
	 * @param pet - pet to be added
	 * @return true if the pet was successfully added
	 */
	public boolean add(Pet pet) {
		if (pet == null) {
			throw new NullPointerException(UI.PET_CANNOT_BE_NULL);
		}
		return this.pets.add(pet);
	}

	/**
	 * Adds all pets in the specified collection.
	 * 
	 * @precondition pets != null && pets does not contain a null element
	 * @postcondition size() = size() + pets.size()
	 * @param pets - collection of pets to be added
	 * @return true if the pets were successfully added
	 */
	public boolean addAll(Collection<? extends Pet> pets) {
		if (pets == null) {
			throw new NullPointerException(UI.PETS_CANNOT_BE_NULL);
		}
		if (pets.contains(null)) {
			throw new NullPointerException(UI.PETS_CANNOT_CONTAIN_NULL);
		}
		return this.pets.addAll(pets);
	}

	/**
	 * Removes the specified pet from this collection.
	 * 
	 * @precondition pets != null
	 * @postcondition size() = size()@prev - 1
	 * @param pet - the pet to be removed
	 * @return true if the pet was removed successfully
	 */
	public boolean remove(Object pet) {
		if (pet == null) {
			throw new NullPointerException(UI.PET_CANNOT_BE_NULL);
		}
		return this.pets.remove(pet);
	}

	/**
	 * Removes the pets in the specified collection from this collection.
	 * 
	 * @precondition pets != null && pets does not contain a null element
	 * @postcondition none
	 * 
	 * @param pets the pets to be removed
	 * 
	 * @return true if the collection has changed
	 */
	public boolean removeAll(Collection<?> pets) {
		if (pets == null) {
			throw new NullPointerException(UI.PETS_CANNOT_BE_NULL);
		}
		if (pets.contains(null)) {
			throw new NullPointerException(UI.PETS_CANNOT_CONTAIN_NULL);
		}
		return this.pets.removeAll(pets);
	}

	/**
	 * Removes the pets not contained in the specified collection.
	 * 
	 * @precondition pets != null && pets does not contain a null element
	 * @postcondition none
	 * @param pets - the pets to be removed
	 * @return true if the collection has changed
	 */
	public boolean retainAll(Collection<?> pets) {
		if (pets == null) {
			throw new NullPointerException(UI.PETS_CANNOT_BE_NULL);
		}
		if (pets.contains(null)) {
			throw new NullPointerException(UI.PETS_CANNOT_CONTAIN_NULL);
		}
		return this.pets.retainAll(pets);
	}

	/**
	 * Returns an array representation of this collection.
	 * 
	 * @precondition none
	 * @postcondition none
	 * @return an array containing all of the elements in this collection
	 */
	public Object[] toArray() {
		return this.pets.toArray();
	}

	/**
	 * Returns an array representation of this collection.
	 * 
	 * @precondition none
	 * @postcondition none
	 * @param <T>   - the generic type
	 * @param array - array to add pets of this collection to
	 * @return an array containing all of the elements in this collection
	 */
	public <T> T[] toArray(T[] array) {
		return this.pets.toArray(array);
	}

	@Override
	public Iterator<Pet> iterator() {
		return this.pets.iterator();
	}

	@Override
	public boolean isEmpty() {
		return this.pets.isEmpty();
	}

	@Override
	public boolean contains(Object pet) {
		if (pet == null) {
			throw new NullPointerException(UI.PET_CANNOT_BE_NULL);
		}
		return this.pets.contains(pet);
	}

	@Override
	public boolean containsAll(Collection<?> pets) {
		if (pets == null) {
			throw new NullPointerException(UI.PETS_CANNOT_BE_NULL);
		}
		if (pets.contains(null)) {
			throw new NullPointerException(UI.PETS_CANNOT_CONTAIN_NULL);
		}
		return this.pets.containsAll(pets);
	}

	@Override
	public void clear() {
		this.pets.clear();
	}

}
