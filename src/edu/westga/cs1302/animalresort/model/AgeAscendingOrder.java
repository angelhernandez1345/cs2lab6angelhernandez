package edu.westga.cs1302.animalresort.model;

import java.util.Comparator;

/**
 * Custom comparator to compare by age, in ascending order.
 * 
 * @author Angel Hernandez
 * @version 10/11/2021
 *
 */
public class AgeAscendingOrder implements Comparator<Pet> {

	@Override
	public int compare(Pet pet1, Pet pet2) {
		if (pet1.getAge() < pet2.getAge()) {
			return -1;
		}
		if (pet1.getAge() > pet2.getAge()) {
			return 1;
		}
		return 0;
	}
}
