package edu.westga.cs1302.animalresort.model;

import java.util.ArrayList;
import java.util.Collection;
import edu.westga.cs1302.animalresort.resources.UI;

/**
 * The Class AnimalResort.
 * 
 * @author	CS1302
 * @version Fall 2021
 */
public class AnimalResort {

	private String name;
	private PetCollection hostedPets;
	private ArrayList<String> petTypes;

	/**
	 * Instantiates a new animal resort.
	 *
	 * @precondition name != null && name not empty
	 * @postcondition getName() == name
	 * 
	 * @param name the name
	 */
	public AnimalResort(String name) {
		if (name == null) {
			throw new IllegalArgumentException(UI.NAME_CANNOT_BE_NULL);
		}

		if (name.isEmpty()) {
			throw new IllegalArgumentException(UI.NAME_CANNOT_BE_EMPTY);
		}

		this.name = name;
		this.hostedPets = new PetCollection();

		this.petTypes = new ArrayList<String>();
		this.petTypes.add("Cat");
		this.petTypes.add("Dog");
		this.petTypes.add("Hamster");
	}

	/**
	 * Gets the name.
	 *
	 * @precondition none
	 * @postcondition none
	 * 
	 * @return the name
	 */
	public String getName() {
		return this.name;
	}

	/**
	 * Returns the number of hosted pets.
	 *
	 * @precondition none
	 * @postcondition none
	 * 
	 * @return the number of hosted pets
	 */
	public int numberPets() {
		return this.hostedPets.size();
	}

	/**
	 * Sets the name.
	 * 
	 * @precondition name != null && name not empty
	 * @postcondition getName() == name
	 *
	 * @param name the new name
	 */
	public void setName(String name) {
		if (name == null) {
			throw new IllegalArgumentException(UI.NAME_CANNOT_BE_NULL);
		}
		if (name.isEmpty()) {
			throw new IllegalArgumentException(UI.NAME_CANNOT_BE_EMPTY);
		}

		this.name = name;
	}

	/**
	 * Gets the hosted pets.
	 * 
	 * @precondition none
	 * @postcondition none
	 *
	 * @return the hosted pets
	 */
	public Collection<Pet> getHostedPets() {
		return this.hostedPets;
	}

	/**
	 * Gets list of types of hosted pets.
	 * 
	 * @precondition none
	 * @postcondition none
	 *
	 * @return the pet types
	 */
	public ArrayList<String> getPetTypes() {
		return this.petTypes;
	}

	/**
	 * Adds the pet to the list of hosted pets.
	 *
	 * @precondition pet != null
	 * @postcondition numberPets() == numberPets()@prev + 1
	 * @param pet - the pet to add
	 * @return true, if the pet was added successfully to the list of hosted pets
	 */
	public boolean addPet(Pet pet) {
		if (pet == null) {
			throw new IllegalArgumentException(UI.PET_CANNOT_BE_NULL);
		}

		return this.hostedPets.add(pet);
	}

	/**
	 * Adds the pet to the list of hosted pets.
	 *
	 * @precondition type != null && (type equals "Cat" or "Dog" or "Hamster") &&
	 *               name != null && name is not empty && age >= 0
	 * @postcondition numberPets() == numberPets()@prev + 1
	 * @param type the name of the pet class
	 * @param name the name of the pet
	 * @param age  the age of the pet
	 * @return true, if the pet was added successfully to the list of hosted pets
	 */
	public boolean addPet(String type, String name, int age) {
		if (type == null) {
			throw new IllegalArgumentException(UI.TYPE_CANNOT_BE_NULL);
		}
		if (!type.equalsIgnoreCase("Cat") && !type.equalsIgnoreCase("Dog") && !type.equalsIgnoreCase("Hamster")) {
			throw new IllegalArgumentException(UI.TYPE_INVALID);
		}

		if (name == null) {
			throw new IllegalArgumentException(UI.NAME_CANNOT_BE_NULL);
		}
		if (name.isEmpty()) {
			throw new IllegalArgumentException(UI.NAME_CANNOT_BE_EMPTY);
		}

		if (age < 0) {
			throw new IllegalArgumentException(UI.AGE_CANNOT_BE_NEGATIVE);
		}

		Pet pet = null;
		if (type.equalsIgnoreCase("Cat")) {
			pet = new Cat(name, age);
		} else if (type.equalsIgnoreCase("Dog")) {
			pet = new Dog(name, age);
		} else {
			pet = new Hamster(name, age);
		}

		return this.hostedPets.add(pet);
	}

	/**
	 * Removes the specified pet from the list of hosted pets.
	 *
	 * @precondition pet != null
	 * @postcondition if found, numberPets() == numberPets()@prev - 1
	 * @param pet pet to delete
	 * @return true if the pet was successfully deleted from the list of hosted pets
	 */
	public boolean removePet(Pet pet) {
		if (pet == null) {
			throw new IllegalArgumentException(UI.PET_CANNOT_BE_NULL);
		}

		return this.hostedPets.remove(pet);
	}

	/**
	 * Gets the report.
	 *
	 * @precondition none
	 * @postcondition none
	 * @return the report
	 */
	public String getReport() {
		String report = "";
		for (Pet pet : this.hostedPets) {
			report += pet.getReport() + System.lineSeparator();
		}
		return report;
	}

	/**
	 * Simulate a day.
	 * 
	 * @precondition none
	 * @postcondition none
	 */
	public void simulateDay() {
		for (Pet pet : this.hostedPets) {
			pet.spendDay();
		}
	}

	/**
	 * Simulate a night.
	 * 
	 * @precondition none
	 * @postcondition none
	 */
	public void simulateNight() {
		for (Pet pet : this.hostedPets) {
			pet.spendNight();
		}
	}

	/**
	 * Gets list of hungry pets.
	 * 
	 * @precondition none
	 * @postcondition none
	 * @return list of hungry pets
	 */
	public ArrayList<Pet> getHungryPets() {
		ArrayList<Pet> hungryPets = new ArrayList<Pet>();
		for (Pet pet : this.hostedPets) {
			if (pet.isHungry()) {
				hungryPets.add(pet);
			}
		}
		return hungryPets;
	}

	/**
	 * Gets list of bored pets.
	 * 
	 * @precondition none
	 * @postcondition none
	 * @return list of bored pets
	 */
	public ArrayList<Pet> getBoredPets() {
		ArrayList<Pet> boredPets = new ArrayList<Pet>();
		for (Pet pet : this.hostedPets) {
			if (pet.isBored()) {
				boredPets.add(pet);

			}
		}
		return boredPets;
	}

	/**
	 * Gets list of hungry pets.
	 * 
	 * @precondition none
	 * @postcondition none
	 * @return list of hungry pets
	 */
	public ArrayList<Pet> getDirtyPets() {
		ArrayList<Pet> dirtyPets = new ArrayList<Pet>();
		for (Pet pet : this.hostedPets) {
			if (pet instanceof Dog && ((Dog) pet).isDirty()) {
				dirtyPets.add(pet);
			}
		}
		return dirtyPets;
	}
}
