package edu.westga.cs1302.animalresort.model;

/**
 * The Class Hamster.
 * 
 * @author	CS1302
 * @version Fall 2021
 */
public class Hamster extends Pet {

	/**
	 * Instantiates a new pet.
	 * 
	 * @precondition name != null && name not empty
	 * @postcondition getName() == name && getAge() == age && isHungry() == false &&
	 *                isTired() == false && isBored() == false
	 * @param name - the name
	 * @param age  - the age
	 */
	public Hamster(String name, int age) {
		super(name, age);
	}

	/**
	 * Returns the sound made by the cat.
	 * 
	 * @precondition none
	 * @postcondition none
	 * 
	 * @return the sound
	 */
	public String getSound() {
		return "zzzzzzz";
	}

	@Override
	public void spendDay() {
		this.sleep();
	}

	@Override
	public void spendNight() {
		this.eat();
		this.setTired(true);
	}

	@Override
	public String toString() {
		return "Hamster: " + this.getName() + " (age " + this.getAge() + ")";
	}
}
