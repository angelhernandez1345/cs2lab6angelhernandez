package edu.westga.cs1302.animalresort.model;

import java.util.Comparator;

/**
 * Specifies that an implementing class must sort some elements.
 * 
 * @author Angel Hernandez
 * @version 10/10/2021
 *
 * @param <Pet> type thats sortable
 */
public interface Sortable<Pet> {
	
	/**
	 * Sorts object.
	 */
	void sort();
	 
	/**
	 * Sorts collection given.
	 * @param pet the object to be compared
	 */
	void sort(Comparator<Pet> pet);
}
