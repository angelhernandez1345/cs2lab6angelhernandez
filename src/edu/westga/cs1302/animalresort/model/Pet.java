package edu.westga.cs1302.animalresort.model;

import edu.westga.cs1302.animalresort.resources.UI;

/**
 * The Class Pet.
 * 
 * @author	CS1302
 * @version Fall 2021
 */
public abstract class Pet {

	private String name;
	private int age;
	private boolean hungry;
	private boolean tired;
	private boolean bored;

	/**
	 * Instantiates a new pet.
	 * 
	 * @precondition name != null && name not empty && age >= 0
	 * @postcondition getName() == name && getAge() == age && isHungry() == false &&
	 *                isTired() == false && isBored() == false
	 * 
	 * @param name - the name
	 * @param age  - the age
	 */
	protected Pet(String name, int age) {
		if (name == null) {
			throw new IllegalArgumentException(UI.NAME_CANNOT_BE_NULL);
		}

		if (name.isEmpty()) {
			throw new IllegalArgumentException(UI.NAME_CANNOT_BE_EMPTY);
		}

		if (age < 0) {
			throw new IllegalArgumentException(UI.AGE_CANNOT_BE_NEGATIVE);
		}

		this.name = name;
		this.age = age;
		this.hungry = false;
		this.tired = false;
		this.bored = false;
	}

	/**
	 * Gets the name.
	 * 
	 * @precondition none
	 * @postcondition none
	 *
	 * @return the name
	 */
	public String getName() {
		return this.name;
	}

	/**
	 * Sets the name.
	 * 
	 * @precondition name != null && name not empty
	 * @postcondition getName() == name
	 * @param name - the new name
	 */
	public void setName(String name) {
		if (name == null) {
			throw new IllegalArgumentException(UI.NAME_CANNOT_BE_NULL);
		}

		if (name.isEmpty()) {
			throw new IllegalArgumentException(UI.NAME_CANNOT_BE_EMPTY);
		}

		this.name = name;
	}

	/**
	 * Gets the age.
	 *
	 * @precondition none
	 * @postcondition none
	 * @return the age
	 */
	public int getAge() {
		return this.age;
	}

	/**
	 * Sets the age.
	 * 
	 * @precondition age >= 0
	 * @postcondition getAge() == age
	 * @param age - the new age
	 */
	public void setAge(int age) {
		if (age < 0) {
			throw new IllegalArgumentException(UI.AGE_CANNOT_BE_NEGATIVE);
		}
		this.age = age;
	}

	/**
	 * Checks if is hungry.
	 *
	 * @precondition none
	 * @postcondition none
	 * @return true, if is hungry
	 */
	public boolean isHungry() {
		return this.hungry;
	}

	/**
	 * Sets whether the pet is hungry.
	 * 
	 * @precondition none
	 * @postcondition isHungry() == hungry
	 * @param hungry - whether pet is hungry
	 */
	public void setHungry(boolean hungry) {
		this.hungry = hungry;
	}

	/**
	 * Checks if is tired.
	 *
	 * @precondition none
	 * @postcondition none
	 * @return true, if is tired
	 */
	public boolean isTired() {
		return this.tired;
	}

	/**
	 * Sets whether the pet is tired.
	 *
	 * @precondition none
	 * @postcondition isTired() == tired
	 * @param tired - whether pet is tired
	 */
	public void setTired(boolean tired) {
		this.tired = tired;
	}

	/**
	 * Checks if is bored.
	 *
	 * @precondition none
	 * @postcondition none
	 * @return true, if is bored
	 */
	public boolean isBored() {
		return this.bored;
	}

	/**
	 * Sets whether the pet is bored.
	 * 
	 * @precondition none
	 * @postcondition isBored() == bored
	 * @param bored - whether pet is bored
	 */
	public void setBored(boolean bored) {
		this.bored = bored;
	}

	/**
	 * Eat.
	 * 
	 * @precondition none
	 * @postcondition isHungry() == false
	 */
	public void eat() {
		this.hungry = false;
	}

	/**
	 * Sleep.
	 * 
	 * @precondition none
	 * @postcondition isTired() == false && isHungry() == true && isBored() == true
	 */
	public void sleep() {
		this.tired = false;
		this.hungry = true;
		this.bored = true;
	}

	/**
	 * Play.
	 *
	 * @precondition none
	 * @postcondition isBored() == false
	 */
	public void play() {
		this.bored = false;
	}

	/**
	 * Returns the sound made by the pet.
	 * 
	 * @precondition none
	 * @postcondition none
	 * @return the sound
	 */
	public abstract String getSound();

	/**
	 * Spend night.
	 */
	public abstract void spendNight();

	/**
	 * Spend day.
	 */
	public abstract void spendDay();

	/**
	 * Gets the report for this pet
	 *
	 * @return the report
	 */
	public String getReport() {
		String report = "My name is " + this.getName() + System.lineSeparator();
		report += this.getSound() + System.lineSeparator();
		if (!this.isHungry() && !this.isTired() && !this.isBored()) {
			report += "* I am content." + System.lineSeparator();
		} else {
			if (this.isHungry()) {
				report += "* I am hungry." + System.lineSeparator();
			}
			if (this.isTired()) {
				report += "* I am tired." + System.lineSeparator();
			}
			if (this.isBored()) {
				report += "* I am bored." + System.lineSeparator();
			}
		}
		return report;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "Pet: " + this.name + " (age " + this.age + ")";
	}
}
