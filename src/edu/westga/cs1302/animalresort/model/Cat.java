
package edu.westga.cs1302.animalresort.model;

/**
 * The Class Cat.
 * 
 * @author	CS1302
 * @version Fall 2021
 */
public class Cat extends Pet {
	
	/**
	 * Instantiates a new pet.
	 *      
	 * @precondition name != null && name not empty
	 * @postcondition getName() == name && getAge() == age
	 *                && isHungry() == false && isTired() == false
	 *                && isBored() == false      
	 * @param name the name
	 * @param age the age
	 */
	public Cat(String name, int age) {
		super(name, age);
	}
	
	/**
	 * Returns the sound made by the cat.
	 * 
	 * @precondition none
	 * @postcondition none
	 * @return the sound
	 */
	@Override
	public String getSound() { 
		if (this.isHungry() || this.isBored()) {
			return "meeeooow";
		} else {
			return "purrrrr";
		}
	}
	
	@Override
	public void spendDay() {
		this.sleep();
	}
	
	@Override
	public void spendNight() {
		this.eat();
		this.play();
		this.setTired(true);
	}
	
	@Override
	public String toString() {
		return "Cat: " + this.getName() + " (age " + this.getAge() + ")";
	}
}
