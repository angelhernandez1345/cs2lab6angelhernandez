package edu.westga.cs1302.animalresort.model;

import java.util.Comparator;

/**
 * Custom comparator to compare by need of care.
 * 
 * @author Angel Hernandez
 * @version 10/11/2021
 *
 */
public class NeedOfCare implements Comparator<Pet> {

	@Override
	public int compare(Pet pet1, Pet pet2) {
		//all hungry and bored pets come first
		
		//followed by all hungry but not bored pets
		
		// all bored pets
		
		//not hungry and not bored
		
		return 0;
	}

}
