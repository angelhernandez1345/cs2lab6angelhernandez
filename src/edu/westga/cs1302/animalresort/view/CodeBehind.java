package edu.westga.cs1302.animalresort.view;

import java.util.Optional;

import edu.westga.cs1302.animalresort.model.Pet;
import edu.westga.cs1302.animalresort.view.converter.ValidPositiveWholeNumberStringConverter;
import javafx.beans.binding.Bindings;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Window;

/**
 * AnimalResortGuiCodeBehind defines the "controller" for Gui.fxml.
 * 
 * @author	CS1302
 * @version Fall 2021
 */
public class CodeBehind {
	private ViewModel viewmodel;
	private ObjectProperty<Pet> selectedPet;
	private final AlertProperty alertProperty;

	private BooleanProperty registerPet;
	private BooleanProperty editPet;

	@FXML
	private AnchorPane pane;

	@FXML
	private ListView<Pet> petListView;

	@FXML
	private TextField nameTextField;

	@FXML
	private TextField ageTextField;

	@FXML
	private ComboBox<String> petTypeComboBox;

	@FXML
	private Button registerButton;

	@FXML
	private Button editButton;

	@FXML
	private Button checkoutButton;

	@FXML
	private Button submitButton;

	@FXML
	private ComboBox<String> sortCriteriaComboBox;

	@FXML
	private Button sortByNameButton;

	@FXML
	private Button sortByAgeButton;

	@FXML
	private Button sortByNeedButton;

	@FXML
	private Button feedButton;

	@FXML
	private Button groomButton;

	@FXML
	private Button playButton;

	@FXML
	private Button closeForNightButton;

	@FXML
	private Label nameLabel;

	@FXML
	private TextArea petInfoTextArea;

	/**
	 * Creates a new AnimalresortGuiCodeBehind object.
	 * 
	 * @precondition none
	 * @postcondition none
	 */
	public CodeBehind() {
		this.alertProperty = new AlertProperty();
		this.registerPet = new SimpleBooleanProperty(false);
		this.editPet = new SimpleBooleanProperty(false);

		this.viewmodel = new ViewModel(this.alertProperty);
		this.selectedPet = new SimpleObjectProperty<Pet>();
		ObservableList<Pet> pets = this.viewmodel.getPets();
		this.petListView = new ListView<Pet>(pets);
	}

	/**
	 * Initializes the GUI components, binding them to the view model properties and
	 * setting their event handlers.
	 * 
	 * @precondition none
	 * @postcondition none
	 * 
	 */
	@FXML
	public void initialize() {
		this.bindToViewModel();
		this.bindDisableProperties();
		this.setPetTypeValues();
		this.setSortCriteriaValues();
		this.setupListenersForPetFormComponents();
		this.setupListenersForSortCriteria();
		this.setupListenerForAlerts();
	}

	private void bindToViewModel() {
		this.nameTextField.textProperty().bindBidirectional(this.viewmodel.nameProperty());
		this.ageTextField.textProperty().bindBidirectional(this.viewmodel.ageProperty(),
				new ValidPositiveWholeNumberStringConverter());
		this.selectedPet.bind(this.petListView.getSelectionModel().selectedItemProperty());

		this.selectedPet.bind(this.petListView.getSelectionModel().selectedItemProperty());
		this.viewmodel.selectedPetProperty().bind(this.selectedPet);

		this.viewmodel.petTypeProperty().bind(this.petTypeComboBox.getSelectionModel().selectedItemProperty());
		this.petInfoTextArea.textProperty().bindBidirectional(this.viewmodel.petInfoProperty());

		this.petListView.setItems(this.viewmodel.getPets());
	}

	private void bindDisableProperties() {
		this.nameTextField.disableProperty().bind(this.submitButton.disableProperty());
		this.ageTextField.disableProperty().bind(this.submitButton.disableProperty());
		this.petTypeComboBox.disableProperty().bind(this.submitButton.disableProperty());

		this.registerButton.disableProperty().bind(Bindings.createBooleanBinding(() -> !this.submitButton.isDisabled(),
				this.submitButton.disableProperty()));
		this.editButton.disableProperty().bind(Bindings.createBooleanBinding(
				() -> !this.submitButton.isDisabled() || this.petListView.getSelectionModel().isEmpty(),
				this.submitButton.disableProperty(), this.petListView.getSelectionModel().selectedItemProperty()));
		this.checkoutButton.disableProperty().bind(Bindings.createBooleanBinding(
				() -> !this.submitButton.isDisabled() || this.petListView.getSelectionModel().isEmpty(),
				this.submitButton.disableProperty(), this.petListView.getSelectionModel().selectedItemProperty()));

		this.submitButton.disableProperty().bind(Bindings.createBooleanBinding(
				() -> !this.registerPet.getValue() && !this.editPet.getValue(), this.registerPet, this.editPet));

		this.feedButton.disableProperty()
				.bind(Bindings.createBooleanBinding(() -> this.petListView.getSelectionModel().isEmpty(),
						this.petListView.getSelectionModel().selectedItemProperty()));
		this.groomButton.disableProperty()
				.bind(Bindings.createBooleanBinding(() -> this.petListView.getSelectionModel().isEmpty(),
						this.petListView.getSelectionModel().selectedItemProperty()));
		this.playButton.disableProperty()
				.bind(Bindings.createBooleanBinding(() -> this.petListView.getSelectionModel().isEmpty(),
						this.petListView.getSelectionModel().selectedItemProperty()));
	}

	private void setPetTypeValues() {
		this.petTypeComboBox.getItems().setAll(this.viewmodel.getPetTypes());
		this.petTypeComboBox.setValue(this.petTypeComboBox.getItems().get(0));
	}

	private void setSortCriteriaValues() {
		ObservableList<String> criteria = FXCollections.observableArrayList();
		criteria.addAll("name", "age", "need for care");
		this.sortCriteriaComboBox.getItems().setAll(criteria);
	}

	private void setupListenersForPetFormComponents() {
		this.ageTextField.textProperty().addListener(new ChangeListener<String>() {
			@Override
			public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
				if (!newValue.matches("\\d{0,3}")) {
					CodeBehind.this.ageTextField.setText(oldValue);
				}
			}
		});

		this.petListView.getSelectionModel().selectedItemProperty().addListener(new ChangeListener<Pet>() {
			@Override
			public void changed(ObservableValue<? extends Pet> observable, Pet oldPet, Pet newPet) {
				if (newPet != null) {
					CodeBehind.this.nameTextField.setText(newPet.getName());
					CodeBehind.this.ageTextField.setText("" + newPet.getAge());
					String petClass = newPet.getClass().getSimpleName();
					CodeBehind.this.petTypeComboBox.setValue(petClass);
					CodeBehind.this.nameLabel.setText(newPet.getName());
					CodeBehind.this.viewmodel.displayPetInfo();
				}
			}
		});
	}

	private void setupListenersForSortCriteria() {
		this.sortCriteriaComboBox.getSelectionModel().selectedItemProperty().addListener(new ChangeListener<String>() {
			@Override
			public void changed(ObservableValue<? extends String> observable, String oldCriteria, String newCriteria) {
				if (newCriteria != null) {
					switch (newCriteria) {
					case "name":
						CodeBehind.this.viewmodel.sortPetsByName();
						break;
					case "age":
						CodeBehind.this.viewmodel.sortPetsByAge();
						break;
					case "need for care":
						CodeBehind.this.viewmodel.sortPetsByNeed();
						break;
					default:
						break;
					}
				}
			}
		});
	}

	private void setupListenerForAlerts() {
		this.alertProperty.addListener(new ChangeListener<Number>() {
			@Override
			public void changed(ObservableValue<? extends Number> observable, Number oldValue, Number newValue) {
				if (newValue.intValue() == AlertProperty.ERROR) {
					CodeBehind.this.showAlert(AlertType.ERROR);
				} else if (newValue.intValue() == AlertProperty.INFORMATION) {
					CodeBehind.this.showAlert(AlertType.INFORMATION);
				} else if (newValue.intValue() == AlertProperty.CONFIRMATION) {
					CodeBehind.this.showAlert(AlertType.CONFIRMATION);
				}
			}
		});
	}

	@FXML
	private void handleRegistration() {
		this.registerPet.setValue(true);
		this.clearPetForm();
	}

	@FXML
	private void handleEdit() {
		this.editPet.setValue(true);
	}

	@FXML
	private void handleCheckout() {
		this.viewmodel.checkoutPet();
		this.clearPetForm();
	}

	@FXML
	void handleSortByName() {
		this.viewmodel.sortPetsByName();
	}

	@FXML
	void handleSortByAge() {
		this.viewmodel.sortPetsByAge();
	}

	@FXML
	void handleSortByNeed() {
		this.viewmodel.sortPetsByNeed();
	}

	@FXML
	private void handleSubmit() {
		if (this.registerPet.getValue()) {
			this.viewmodel.registerPet();
			this.registerPet.setValue(false);
		} else if (this.editPet.getValue()) {
			this.viewmodel.editPet();
			this.editPet.setValue(false);
		}
		this.clearPetForm();
	}

	@FXML
	private void handleFeedPet() {
		this.viewmodel.feedPet();
		this.viewmodel.displayPetInfo();
	}

	@FXML
	private void handleGroomPet() {
		this.viewmodel.groomPet();
		this.viewmodel.displayPetInfo();
	}

	@FXML
	void handlePlayWithPet() {
		this.viewmodel.playWithPet();
		this.viewmodel.displayPetInfo();
	}

	@FXML
	void handleCloseForNight() {
		this.viewmodel.closeForNight();
		this.viewmodel.displayPetInfo();
	}

	/**
	 * Show error alert dialog.
	 * 
	 * @param alertType - the type of this alert
	 */
	private void showAlert(Alert.AlertType alertType) {
		Alert alert = new Alert(alertType);
		Window owner = this.pane.getScene().getWindow();
		alert.initOwner(owner);
		if (!this.alertProperty.getTitle().isEmpty()) {
			alert.setTitle(this.alertProperty.getTitle());
		}
		alert.setHeaderText(this.alertProperty.getHeader());
		alert.setContentText(this.alertProperty.getContent());
		Optional<ButtonType> alertResult = alert.showAndWait();
		if (alertType == AlertType.CONFIRMATION && alertResult.get() == ButtonType.CANCEL) {
			this.alertProperty.setResult("cancel");
		} else {
			this.alertProperty.setResult("ok");
		}
		this.alertProperty.setType(AlertProperty.NO_ALERT);
	}

	private void clearPetForm() {
		this.nameTextField.setText("");
		this.ageTextField.setText("0");
		this.petTypeComboBox.getSelectionModel().select(0);
		this.petListView.getSelectionModel().clearSelection();
	}
}