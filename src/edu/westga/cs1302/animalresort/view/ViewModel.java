package edu.westga.cs1302.animalresort.view;

import java.util.ArrayList;
import edu.westga.cs1302.animalresort.model.AnimalResort;
import edu.westga.cs1302.animalresort.model.Dog;
import edu.westga.cs1302.animalresort.model.Pet;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

/**
 * AnimalResortViewModel mediates between the view and the rest of the program.
 * 
 * @author	CS1302
 * @version Fall 2021
 */
public class ViewModel {

	private StringProperty name;
	private IntegerProperty age;
	private StringProperty petType;
	private ObservableList<Pet> petList;
	private ObjectProperty<Pet> selectedPet;
	private StringProperty petInfo;
	private AlertProperty alertProperty;

	private AnimalResort resort;

	/**
	 * Creates a new MusicLibraryViewModel object with its properties initialized.
	 * 
	 * @precondition alertProperty != null
	 * @postcondition none
	 * 
	 * @param alertProperty alert property to notify the GUI of alerts
	 */
	public ViewModel(AlertProperty alertProperty) {
		if (alertProperty == null) {
			throw new IllegalArgumentException("ViewModel requires non-null alert property.");
		}
		this.name = new SimpleStringProperty();
		this.age = new SimpleIntegerProperty();
		this.petType = new SimpleStringProperty();
		this.petList = FXCollections.observableArrayList();
		this.selectedPet = new SimpleObjectProperty<Pet>();
		this.petInfo = new SimpleStringProperty();
		this.alertProperty = alertProperty;
		this.resort = new AnimalResort("Happy Pets");

		this.petList.setAll(this.resort.getHostedPets());
	}

	/**
	 * Returns name property.
	 * 
	 * @precondition none
	 * @postcondition none
	 *
	 * @return the name property
	 */
	public StringProperty nameProperty() {
		return this.name;
	}

	/**
	 * Returns name property.
	 *
	 * @precondition none
	 * @postcondition none
	 *
	 * @return the name property
	 */
	public IntegerProperty ageProperty() {
		return this.age;
	}

	/**
	 * Returns pet type property.
	 * 
	 * @precondition none
	 * @postcondition none
	 *
	 * @return the pet type property
	 */
	public StringProperty petTypeProperty() {
		return this.petType;
	}

	/**
	 * Returns selected pet property.
	 * 
	 * @precondition none
	 * @postcondition none
	 *
	 * @return the pet type property
	 */
	public ObjectProperty<Pet> selectedPetProperty() {
		return this.selectedPet;
	}

	/**
	 * Returns pet info property.
	 * 
	 * @precondition none
	 * @postcondition none
	 *
	 * @return the pet info property
	 */
	public StringProperty petInfoProperty() {
		return this.petInfo;
	}

	/**
	 * Returns list of pets.
	 * 
	 * @precondition none
	 * @postcondition none
	 *
	 * @return the name property
	 */
	public ObservableList<Pet> getPets() {
		return this.petList;
	}

	/**
	 * Returns a list of possible pet types.
	 * 
	 * @precondition none
	 * @postcondition none
	 *
	 * @return the types of possible pets
	 */
	public ArrayList<String> getPetTypes() {
		return this.resort.getPetTypes();
	}

	/**
	 * Adds a pet that has been entered in the registration form.
	 * 
	 * @precondition none
	 * @postcondition resort.getHostedPets().numberPets() ==
	 *                resort.getHostedPets().numberPets()@prev + 1
	 */
	public void registerPet() {
		try {
			String name = this.name.getValue();
			int age = this.age.getValue();
			String petType = this.petType.getValue();

			this.resort.addPet(petType, name, age);
			this.petList.setAll(this.resort.getHostedPets());
			this.alertProperty.set(AlertProperty.INFORMATION, "Registration successful",
					"Pet " + name + " has been registered.");
		} catch (Exception ex) {
			this.alertProperty.set(AlertProperty.ERROR, "Registration Error",
					"Entered data is invalid: " + ex.getMessage());
		}
	}

	/**
	 * Updates a pet using the data from the registration form.
	 * 
	 * @precondition none
	 * @postcondition resort.getHostedPets() has been updated
	 */
	public void editPet() {
		String name = this.name.getValue();
		int age = this.age.getValue();
		String petType = this.petType.getValue().toLowerCase();

		Pet pet = this.selectedPet.getValue();

		try {
			String className = pet.getClass().getName().toLowerCase();

			boolean updated = false;
			if (className.contains(petType)) {
				pet.setName(name);
				pet.setAge(age);
				updated = true;
			} else if (this.resort.addPet(petType, name, age)) {
				this.resort.removePet(pet);
				updated = true;
			}

			if (updated) {
				this.petList.setAll(this.resort.getHostedPets());
				this.alertProperty.set(AlertProperty.INFORMATION, "Update successful",
						"The pet " + name + " has been updated.");
			} else {
				this.alertProperty.set(AlertProperty.INFORMATION, "Update error",
						"Error countered updating pet: " + name);
			}

		} catch (Exception ex) {
			this.alertProperty.set(AlertProperty.ERROR, "Update error", ex.getMessage());
		}
	}

	/**
	 * Removes the selected pet.
	 * 
	 * @precondition none
	 * @postcondition resort.getHostedPets().numberPets() ==
	 *                resort.getHostedPets().numberPets()@prev - 1
	 */
	public void checkoutPet() {
		Pet pet = this.selectedPet.getValue();

		if (pet != null) {
			this.alertProperty.set(AlertProperty.CONFIRMATION, "Checkout Confirmation",
					"Are you sure you want to check out " + pet.getName() + "?");
			if (!this.alertProperty.getResult().equals("cancel")) {
				this.resort.removePet(pet);
				this.petList.setAll(this.resort.getHostedPets());

				this.alertProperty.set(AlertProperty.INFORMATION, "Checkout successful",
						"The pet " + pet.getName() + " has been checked out.");
			}
		} else {
			this.alertProperty.set(AlertProperty.INFORMATION, "Checkout failed", "No pet has been selected");
		}
	}

	/**
	 * Sorts the resort's pets.
	 *
	 * @precondition none
	 * @postcondition none
	 */
	public void sortPetsByName() {
		this.petList.setAll(this.resort.getHostedPets());
	}

	/**
	 * Sorts the resort's pets by age.
	 *
	 * @precondition none
	 * @postcondition none
	 */
	public void sortPetsByAge() {
		this.petList.setAll(this.resort.getHostedPets());
	}

	/**
	 * Sorts the resort's pets by need.
	 *
	 * @precondition none
	 * @postcondition none
	 */
	public void sortPetsByNeed() {
		this.petList.setAll(this.resort.getHostedPets());
	}

	/**
	 * Feeds the selected pet.
	 * 
	 * @precondition none
	 * @postcondition none
	 */
	public void feedPet() {
		Pet pet = this.selectedPet.getValue();
		if (pet == null) {
			this.alertProperty.set(AlertProperty.INFORMATION, "Feeding failed", "No pet has been selected.");
		} else {
			pet.eat();
		}
	}

	/**
	 * Grooms the selected pet.
	 * 
	 * @precondition none
	 * @postcondition none
	 */
	public void groomPet() {
		Pet pet = this.selectedPet.getValue();
		if (pet == null) {
			this.alertProperty.set(AlertProperty.INFORMATION, "Grooming failed", "No pet has been selected.");
		} else {
			if (pet instanceof Dog) {
				((Dog) pet).setDirty(false);
			} else {
				this.alertProperty.set(AlertProperty.INFORMATION, "Grooming failed",
						"The selected pet does not like to be groomed.");
			}
		}
	}

	/**
	 * Plays with the selected pet.
	 * 
	 * @precondition none
	 * @postcondition none
	 */
	public void playWithPet() {
		Pet pet = this.selectedPet.getValue();
		if (pet == null) {
			this.alertProperty.set(AlertProperty.INFORMATION, "Playing failed", "No pet has been selected.");
		} else {
			pet.play();
		}
	}

	/**
	 * Checks whether all pets are content. If so, a night is simulated.
	 * 
	 * @precondition none
	 * @postcondition none
	 */
	public void closeForNight() {
		if (this.resort.getHungryPets().isEmpty() && this.resort.getBoredPets().isEmpty()
				&& this.resort.getDirtyPets().isEmpty()) {
			this.resort.simulateNight();
		} else {
			this.alertProperty.set(AlertProperty.INFORMATION, "Closing for the Night", "You cannot go home yet!",
					"Some pets are not cared for.");
		}
	}

	/**
	 * Displays pet info.
	 */
	public void displayPetInfo() {
		Pet pet = this.selectedPet.getValue();
		if (pet != null) {
			String petReport = pet.getReport();
			this.petInfo.set(petReport);
		}
	}
}
